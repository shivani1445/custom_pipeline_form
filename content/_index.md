## Select a Feature Model to view

  <div id="pipeline-form">
   <form action="#" method="post">
     <select id="projectid" style = "width: -webkit-fill-available; border-radius: 5px; border-color: blue; margin: 2.5px" >
      <option selected>Select a Project</option>
      <option>Project A</option>
      <option>Project B</option>
      <option>Project C</option>
     </select>
     </br>
     <select id="folderid" style = "width: -webkit-fill-available; border-radius: 5px; border-color: blue;  margin: 2.5px" >
      <option selected>Select a Version</option>
      <option>v1.0</option>
      <option>v2.0</option>
      <option>v3.0</option>
     </select>
     </br></br>
     <button type="button" id="api-example">Open</button>
   </form>
  </div>
  <div id="pipeline-after" style="display:none;">
   <p id="api-output">Please stand by .. </p>
  </div>
